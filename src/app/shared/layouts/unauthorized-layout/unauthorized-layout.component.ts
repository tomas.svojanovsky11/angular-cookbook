import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-unauthorized-layout',
  templateUrl: './unauthorized-layout.component.html',
  styleUrls: ['./unauthorized-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnauthorizedLayoutComponent implements OnInit {

  constructor () {
  }

  ngOnInit (): void {
  }

}
