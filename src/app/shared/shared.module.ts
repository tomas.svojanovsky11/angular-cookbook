import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'

import { ButtonComponent } from './components/button/button.component'
import { InputComponent } from './components/input/input.component'
import { UnauthorizedLayoutComponent } from './layouts/unauthorized-layout/unauthorized-layout.component'

@NgModule({
  declarations: [
    ButtonComponent,
    InputComponent,
    UnauthorizedLayoutComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    ButtonComponent,
    InputComponent,
  ]
})
export class SharedModule {

}
