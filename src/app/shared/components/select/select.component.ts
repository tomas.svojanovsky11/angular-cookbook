import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output
} from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

export interface SelectItem {
  value: string;
  label: string;
}

@Component({
  selector: 'app-select',
  templateUrl: 'select.component.html',
  styleUrls: ['select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => SelectComponent),
    },
  ],
})
export class SelectComponent implements ControlValueAccessor {
  @Input() set options (options: SelectItem[]) {
    this.length = options.length
    this._options = options
  }

  @Input() selector = ''

  @Input() selected?: SelectItem

  @Input() placeholder = ''

  @Output() outSelect = new EventEmitter<SelectItem>()

  // tslint:disable-next-line:variable-name
  _options: SelectItem[] = []

  length = 0

  hidden = true

  constructor (private cdr: ChangeDetectorRef) {
  }

  onClick () {
    if (this.length > 0) {
      this.hidden = !this.hidden
    }
  }

  onSelect (option: SelectItem) {
    this.outSelect.emit(option)
    this.onClick()
  }

  writeValue (value: any) {
    this.selected = value || undefined
    this.cdr.markForCheck()
  }

  propagateChange = (_: any) => {
  }

  registerOnChange (fn: any) {
    this.propagateChange = fn
  }

  registerOnTouched () {
  }
}
