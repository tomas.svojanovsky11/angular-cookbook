import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core'

export type ButtonType = 'submit' | 'reset' | 'button';

export type ButtonSize = 'normal' | 'small';

@Component({
  selector: 'app-button',
  templateUrl: 'button.component.html',
  styleUrls: ['button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonComponent {
  @Input() label = ''

  @Input() type: ButtonType = 'button'

  @Input() active = false

  @Input() size: ButtonSize = 'normal'

  @Input() disabled = false

  @Output() outClick = new EventEmitter<void>()

  onClick () {
    this.outClick.emit()
  }
}
