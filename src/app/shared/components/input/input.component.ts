import { ChangeDetectionStrategy, Component, EventEmitter, forwardRef, Input, Output } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

export type InputType = 'text' | 'password' | 'email';

@Component({
  selector: 'app-input',
  templateUrl: 'input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => InputComponent),
    },
  ],
})
export class InputComponent implements ControlValueAccessor {
  @Input() label = ''

  @Input() placeholder = ''

  @Input() type: InputType = 'text'

  @Output() outText = new EventEmitter()

  @Input() set value (value: string) {
    this._value = value
    this.onChange(value)
    this.onTouched()
  }

  // tslint:disable-next-line:variable-name
  _value = ''

  onChange: any = () => {
  }
  onTouched: any = () => {
  }

  writeValue (value: string): void {
    this._value = value || ''
  }

  registerOnChange (fn: any) {
    this.onChange = fn
  }

  registerOnTouched (fn: any) {
    this.onTouched = fn
  }

  onChangeValue ($event: any) {
    this.onChange($event.target.value)
  }
}
