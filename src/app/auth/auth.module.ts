import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms'

import { SharedModule } from '../shared/shared.module'
import { LoginComponent } from './pages/login/login.component'
import { AuthRoutingModule } from './auth-routing.module'

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    SharedModule,
    AuthRoutingModule,
    ReactiveFormsModule,
  ],
})
export class AuthModule {

}
