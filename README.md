# AngularCookbook

## Components

* https://angular.io/guide/component-overview
* https://angular.io/guide/lifecycle-hooks
* https://angular.io/guide/component-styles

## Forms

* https://angular.io/guide/forms-overview
* https://angular.io/guide/reactive-forms
* https://angular.io/guide/form-validation

# SCSS

* https://www.youtube.com/watch?v=_a5j7KoflTs&t=1s
* https://medium.com/ampersand-academy/partials-in-sass-scss-5aae1beee414
* https://sass-lang.com/documentation/variables
